$(document).ready(function(){
console.log("popup.js loaded");
//var car_position;
if (!localStorage.hasOwnProperty("permission")) {
    window.open('permission.html');
}

 var port = chrome.extension.connect({
      name: "Sample Communication"
 });
 port.postMessage("Hi BackGround");
 port.onMessage.addListener(function(msg) {
      console.log("message recieved" + msg);
 });

document.getElementById("btn_cut_call").addEventListener("click", chrome.extension.getBackgroundPage().hangup);
document.getElementById("btn_hangup").addEventListener("click", chrome.extension.getBackgroundPage().hangup);
document.getElementById("btn_answer").addEventListener("click", chrome.extension.getBackgroundPage().takeCall);
document.getElementById("btn_register_client").addEventListener("click",register_client);
document.getElementById("btn_make_call").addEventListener("click", start_call);
document.getElementById("btn_select_num").addEventListener("click", set_voice_url);
document.getElementById("bottom_button_settings").addEventListener("click", chrome.extension.getBackgroundPage().update_settings);
document.getElementById("bottom_button_home").addEventListener("click", chrome.extension.getBackgroundPage().back_settings);
document.getElementById("reset_app").addEventListener("click",reset_app);
document.getElementById("btn_select_num_reset").addEventListener("click",reset_app);
document.getElementById("btn_set_url").addEventListener("click", set_crm);
//document.getElementById("btn_set_url").addEventListener("click", set_callback_url);
document.getElementById("btn_skip_url").addEventListener("click", skip_callback_url);
document.getElementById("btn_update_url").addEventListener("click", update_crm);
document.getElementById("bottom_button_log").addEventListener("click", chrome.extension.getBackgroundPage().get_call_log);
document.getElementById("go_reniew").addEventListener("click",expiry_buttons);
document.getElementById("check_reniew").addEventListener("click",expiry_buttons);
document.getElementById("crm_selection").addEventListener("change",select_crm);
document.getElementById("crm_selection_set").addEventListener("change",select_crm);
document.getElementById("sign_in_chrome").addEventListener("click",chrome.extension.getBackgroundPage().chrome_signIn);
document.getElementById("allow_microphone").addEventListener("click",request_permission);

/***********************************update value to text boxes with temp values*****************************/

document.getElementById("sid").value=chrome.extension.getBackgroundPage().temp_sid;
document.getElementById("token").value=chrome.extension.getBackgroundPage().temp_token;
document.getElementById("client_name").value=chrome.extension.getBackgroundPage().temp_client;
document.getElementById("api_key_1").value=chrome.extension.getBackgroundPage().temp_api_key_1;
document.getElementById("api_key_1_set").value=chrome.extension.getBackgroundPage().temp_api_key_1;
document.getElementById("agile_domain").value=chrome.extension.getBackgroundPage().temp_agile_domain;
document.getElementById("agile_domain_set").value=chrome.extension.getBackgroundPage().temp_agile_domain;
document.getElementById("api_key_2_usr").value=chrome.extension.getBackgroundPage().temp_api_key_2_usr;
document.getElementById("api_key_2_usr_set").value=chrome.extension.getBackgroundPage().temp_api_key_2_usr;
document.getElementById("api_key_2_key").value=chrome.extension.getBackgroundPage().temp_api_key_2_key;
document.getElementById("api_key_2_key_set").value=chrome.extension.getBackgroundPage().temp_api_key_2_key;
document.getElementById("api_key_3").value=chrome.extension.getBackgroundPage().temp_api_key_3;
document.getElementById("api_key_3_set").value=chrome.extension.getBackgroundPage().temp_api_key_3;
document.getElementById("zoho_usr").value=chrome.extension.getBackgroundPage().temp_zoho_usr;
document.getElementById("zoho_usr_set").value=chrome.extension.getBackgroundPage().temp_zoho_usr;
document.getElementById("zoho_pwd").value=chrome.extension.getBackgroundPage().temp_zoho_pwd;
document.getElementById("zoho_pwd_set").value=chrome.extension.getBackgroundPage().temp_zoho_pwd;
document.getElementById("callback_url").value=chrome.extension.getBackgroundPage().temp_callback_url;
document.getElementById("callback_url_set").value=chrome.extension.getBackgroundPage().temp_callback_url;
document.getElementById("domain_url").value=chrome.extension.getBackgroundPage().temp_domain_url;
document.getElementById("domain_url_set").value=chrome.extension.getBackgroundPage().temp_domain_url;


var timeoutId1 = 0;
var timeinteval=0;

$("input").on('keyup paste',function(e){
    var id=$(this).attr('id');
    //console.log("key up or paste on",id);
    setTimeout(function(){
         //console.log(e);
          var value=document.getElementById(id).value;
          //console.log(value);
          switch (id) {
            case 'sid':
              chrome.extension.getBackgroundPage().temp_sid=value;
              break;
            case 'token':
              chrome.extension.getBackgroundPage().temp_token=value;
              break;
            case 'client_name':
              chrome.extension.getBackgroundPage().temp_client=value;
              break;
            case 'api_key_1':
            case 'api_key_1_set':
              chrome.extension.getBackgroundPage().temp_api_key_1=value;
              break;
            case 'agile_domain':
            case 'agile_domain_set':
              chrome.extension.getBackgroundPage().temp_agile_domain=value;
              break;
            case 'api_key_2_usr':
            case 'api_key_2_usr_set':
              chrome.extension.getBackgroundPage().temp_api_key_2_usr=value;
              break;
            case 'api_key_2_key':
            case 'api_key_2_key_set':
              chrome.extension.getBackgroundPage().temp_api_key_2_key=value;
              break;
            case 'api_key_3':
            case 'api_key_3_set':
              chrome.extension.getBackgroundPage().temp_api_key_3=value;
              break;
            case 'zoho_usr':
            case 'zoho_usr_set':
              chrome.extension.getBackgroundPage().temp_zoho_usr=value;
              break;
            case 'zoho_pwd':
            case 'zoho_pwd_set':
              chrome.extension.getBackgroundPage().temp_zoho_pwd=value;
              break;
            case 'callback_url':
            case 'callback_url_set':
              chrome.extension.getBackgroundPage().temp_callback_url=value;
              break;
            case 'domain_url':
            case 'domain_url_set':
              chrome.extension.getBackgroundPage().temp_domain_url=value;
              break;
            default:
              break;
          }
    },100);
});

/***********************************long press on backspace*****************************/

$('#back_space').on('mousedown', function() {
      clear_one();
    timeoutId1 = setTimeout(function(){
      // console.log("back_space clicked for multiple deletion.");
       timeinteval=setInterval(clear_multiple, 200);
    }, 1000);
}).on('mouseup', function() {
    //console.log("back_space leaved.");
    clearTimeout(timeoutId1);
    clearInterval(timeinteval);
});

function clear_one()
{
    //console.log("back_space clicked for clear one");

    var val=$("#phone_number").val();
    val=val.substr(0,val.length-1);
    var num=chrome.extension.getBackgroundPage().format_number(val);
    $("#phone_number").val(num);
    if( $("#phone_number").val()=="")
    {
      //$("#phone_number").removeAttr('disabled');
       //document.getElementById("phone_number").style.background= 'none';
      // console.log("back_space leaved.");
      clearInterval(timeinteval);
      clearTimeout(timeoutId1);
      chrome.extension.getBackgroundPage().hide_view("back_space");
      
    }
}

function clear_multiple()
{
    //console.log("multiple clearance");

    var val=$("#phone_number").val();
    val=val.substr(0,val.length-1);
    var num=chrome.extension.getBackgroundPage().format_number(val);
    $("#phone_number").val(num);
    if( $("#phone_number").val()=="")
    {
      //$("#phone_number").removeAttr('disabled');
       //document.getElementById("phone_number").style.background= 'none';
     // console.log("back_space leaved.");
      clearInterval(timeinteval);
      clearTimeout(timeoutId1);
      chrome.extension.getBackgroundPage().hide_view("back_space");
      
    }
}
/***************************help click events*******************************************/

$(".help_url").on('click',function()
{
    var help_id=$(this).attr('id');
    chrome.extension.getBackgroundPage().help_open(help_id);
});


/*************************iphone number click events handling***************************/


var timeoutId = 0;

$('#plus_zero').on('mousedown', function() {
    timeoutId = setTimeout(myFunction, 1000);
}).on('mouseup mouseleave', function() {
    clearTimeout(timeoutId);
});

function myFunction()
{
    var num=$("#phone_number").val();
    num=num.substr(0,num.length-1);
    //console.log("removing last char:"+num);
    num=num+"+";
   // console.log("adding + char:"+num);
    num=chrome.extension.getBackgroundPage().format_number(num);
      $("#phone_number").val(num); 
      chrome.extension.getBackgroundPage().show_view("back_space");
}



$(".num").on('mousedown',function(){

	//$("#phone_number").attr('disabled','disabled');

    var value=$(this).attr('rel');
    //console.log("entered value:"+value);
    if(value!="" && value!=null)
    {
     //var num=$("#phone_number").val()+value;
      var num=chrome.extension.getBackgroundPage().format_number($("#phone_number").val()+value);
      $("#phone_number").val(num);
    }
    if($("#phone_number").val!="")
    {
      chrome.extension.getBackgroundPage().show_view("back_space");
    }

});

$("#phone_number").on('keyup',function(e){
    /*var chr = String.fromCharCode(e.keyCode);*/
    //console.log("key pressed:"+e.keyCode)
    var value=$("#phone_number").val();
    if(value!="" && value!=null)
    {
      //var num=$("#phone_number").val();
      var num=chrome.extension.getBackgroundPage().format_number(value);
      $("#phone_number").val(num);
    }
    if($("#phone_number").val()!="")
    {
      chrome.extension.getBackgroundPage().show_view("back_space");
    }
    else
    {
      chrome.extension.getBackgroundPage().hide_view("back_space");
    }

});



/********************************************call log click to call************************************************/

$("#recent_log").on("click", ".log_item" , function(){

     // console.log("log_clicked.");
      var number_to_call=$(this).attr('val');
     // console.log("number_to_call:"+number_to_call);
      chrome.extension.getBackgroundPage().call(number_to_call);
  });





/***********************************************************************************************************/
function start_call(){
  var to_number = document.getElementById("phone_number").value;
  if(to_number==""){
    /*var log_content = document.getElementById("log").innerHTML;
    document.getElementById("log").innerHTML = "Please enter a number";
    setTimeout(function(){
      document.getElementById("log").innerHTML = log_content;
    },2000);*/
  }else{
      chrome.extension.getBackgroundPage().call(to_number);
  }
}

function register_client(){
  var sid = document.getElementById("sid").value;
  sid=sid.trim();
  var token = document.getElementById("token").value;
  token=token.trim();
  var client= document.getElementById("client_name").value;
  client=client.trim();
  //console.log("register_client clicked",/^\w+$/.test(client));
  //alert("sid:"+sid+"\rtoken:"+token);

  if(sid=="" || token=="" || client==""){

    change_log("log_div","log","Form incomplete");
    /*var log_content = document.getElementById("log").innerHTML;
    console.log("log value",log_content); 
    document.getElementById("log").innerHTML = "Form incomplete.";
    setTimeout(function(){
      document.getElementById("log").innerHTML = log_content;
    },2000);*/
  }
  else if(!/^\w+$/.test(client))
  {
    change_log("log_div","log","Client name should not contain spaces <br> or special characters.");
  }
  else{
      chrome.extension.getBackgroundPage().register_client(sid,token,client);
  }

}

function set_voice_url()
{
  var number=document.getElementById("number_list").value;
  //console.log("setting voice url for the number:"+number+"with client name"+localStorage["identity"]);
  if(number!=null)
  {
    chrome.extension.getBackgroundPage().set_voice_url(number);
  }
  else
  {
    change_log("log_div","log","No numbers available");
  }
}

function set_crm()
{
  update_callback_url("crm_selection_set","domain_url_set","hit_on_ring_set","callback_url_set","api_key_1_set","api_key_2_usr_set","api_key_2_key_set","agile_domain_set","api_key_3_set","salesforce_instance_name_set","salesforce_client_id_set","salesforce_client_secret_set","api_key_4_set","zoho_usr_set","zoho_pwd_set","set");
}
function update_crm()
{
  update_callback_url("crm_selection","domain_url","hit_on_ring","callback_url","api_key_1","api_key_2_usr","api_key_2_key","agile_domain","api_key_3","salesforce_instance_name","salesforce_client_id","salesforce_client_secret","api_key_4","zoho_usr","zoho_pwd","update");
}
function skip_callback_url()
{
  //console.log("skipping crm settings");
  clear_input_in_settingsUI();
  chrome.extension.getBackgroundPage().skip_callback_url();
}
function update_callback_url(crm_type_tag,domain_tag,hit_on_ring_tag,callback_url_tag,api_key_1_tag,api_key_2_usr_tag,api_key_2_key_tag,agile_domain_tag,api_key_3_tag,salesforce_instance_name_tag,salesforce_client_id_tag,salesforce_client_secret_tag,api_key_4_tag,zoho_usr_tag,zoho_pwd_tag,update_or_set){
  var re = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
  var test_ok=false;
  var checked="unchecked";
  var crm_type=document.getElementById(crm_type_tag).value;
  var domain_url=document.getElementById(domain_tag).value;
      domain_url=domain_url.trim();
 // console.log("updating domain_url:"+domain_url);
  if(document.getElementById(hit_on_ring_tag).checked)
  {
    checked="checked";
  }
  if (re.test(domain_url) || domain_url=="")
  {
    test_ok=true;
  }
  else
  {
      change_log("log_div","log","enter valid CRM domain.");
      return false;
  }

  switch(crm_type)
  {
    case 'nil': 
                var url=document.getElementById(callback_url_tag).value;
                url=url.trim();
               // console.log("updating callback_url:"+url);
                if (!re.test(url) && url!="")
                {
                      change_log("log_div","log","enter valid address to fetch CRM details.");
                      test_ok=false;
                      return false;
                }
                chrome.extension.getBackgroundPage().update_CRM(crm_type,domain_url,checked,url,null,null,null,update_or_set);
                break;
    case '1'  :
               var api_key=document.getElementById(api_key_1_tag).value;
               api_key=api_key.trim();
              // console.log("updating api_key for insightly:",api_key);
               if(api_key == "")
               {
                  change_log("log_div","log","enter valid API key for CRM.");
                  test_ok=false;
                  return false;
               }
               chrome.extension.getBackgroundPage().update_CRM(crm_type,"insightly.com",checked,api_key,null,null,null,update_or_set);
               break;
    case '2'  :
               var api_key_2_usr=document.getElementById(api_key_2_usr_tag).value;
               api_key_2_usr=api_key_2_usr.trim();
               var api_key_2_key=document.getElementById(api_key_2_key_tag).value;
               api_key_2_key=api_key_2_key.trim();
               var api_key_2=api_key_2_usr+":"+api_key_2_key;
               var agile_domain=document.getElementById(agile_domain_tag).value;
               agile_domain=agile_domain.trim();
               //console.log("updating api_key_2 for agile crm:",api_key_2);
               //console.log("updating agile_domain :",agile_domain);
               if(api_key_2_usr == "" || api_key_2_key == "" || agile_domain=="")
               {
                  change_log("log_div","log","enter valid CRM details.");
                  test_ok=false;
                  return false;
               }
               chrome.extension.getBackgroundPage().update_CRM(crm_type,"agilecrm.com",checked,api_key_2,agile_domain,null,null,update_or_set);
               break;
    case '3'  :
               var api_key=document.getElementById(api_key_3_tag).value;
               api_key=api_key.trim();
               //console.log("updating api_key for hubspot:",api_key);
               if(api_key == "")
               {
                  change_log("log_div","log","enter valid API key for CRM.");
                  test_ok=false;
                  return false;
               }
               chrome.extension.getBackgroundPage().update_CRM(crm_type,"app.hubspot.com",checked,api_key,null,null,null,update_or_set);
               break;
    case '4'  :
               var instance_name=document.getElementById(salesforce_instance_name_tag).value;
               instance_name=instance_name.trim();
               var token=document.getElementById(api_key_4_tag).value;
               token=token.trim();
               var client_id=document.getElementById(salesforce_client_id_tag).value;
               client_id=client_id.trim();
               var client_secret=document.getElementById(salesforce_client_secret_tag).value;
               client_secret=client_secret.trim();
               //console.log("updating details for salesforce:");
               //console.log(client_id);
               //console.log(client_secret);
               //console.log(token);
               if(token == "" || client_id == "" || client_secret == "" || instance_name=="")
               {
                  change_log("log_div","log","enter valid salesforce details.");
                  test_ok=false;
                  return false;
               }
               chrome.extension.getBackgroundPage().update_CRM(crm_type,domain_url,checked,client_id,client_secret,token,instance_name,update_or_set);
               break;
    case '5'  :
               /*var api_key=document.getElementById(api_key_5_tag).value;
               api_key=api_key.trim();*/
               var zoho_usr=document.getElementById(zoho_usr_tag).value;
               zoho_usr=zoho_usr.trim();
               var zoho_pwd=document.getElementById(zoho_pwd_tag).value;
               zoho_pwd=zoho_pwd.trim();
               //console.log("updating auth token for Zoho:",api_key);
               if(zoho_usr == "" || zoho_pwd=="")
               {
                  change_log("log_div","log","enter valid details for ZOHO.");
                  test_ok=false;
                  return false;
               }
               //get_zoho_token(zoho_usr,zoho_pwd);
               get_zoho_token(crm_type,"zoho.com",checked,zoho_usr,zoho_pwd,update_or_set);
               
               //chrome.extension.getBackgroundPage().update_CRM(crm_type,domain_url,checked,api_key,null,null,null,update_or_set);
               break;
    default: //console.log("No crm type selected.");
             if(checked=="checked" || crm_type_tag=="crm_selection_set")
             {
                  change_log("log_div","log","Select a CRM first.");
                  test_ok=false;
                  return false;
             }
             /*if(domain_url=="" && crm_type_tag=="crm_selection_set")
             {
                  change_log("log_div","log","Set CRM or Domain first.");
                  test_ok=false;
                  return false;
             }*/
             chrome.extension.getBackgroundPage().update_CRM(crm_type,"",checked,null,null,null,null,update_or_set);
             break;

  }
  if(crm_type!="5")
     clear_input_in_settingsUI();
    //console.log("test_ok for updating settings:",test_ok);
}


function reset_app()
{
  chrome.extension.getBackgroundPage().reset_app();
  location.reload(); 
  setTimeout(function(){
    chrome.extension.getBackgroundPage().reload_app();
  },2000);
}

function change_log(div_id,id,text)
{
        //document.getElementById("log_div").style.display = 'inline';
        document.getElementById(div_id).style.display = "inline";
        document.getElementById(id).innerHTML = text;
        setTimeout(function(){
          document.getElementById(id).innerHTML = "";
          document.getElementById(div_id).style.display = "none";
          //document.getElementById("log_div").style.display = 'none';
        },2000);
}

function expiry_buttons(){
  var id=$(this).attr('id');
  //console.log("button pressed id is"+id);
  chrome.extension.getBackgroundPage().expiry_buttons(id);
  
}

function select_crm()
{
  //console.log("on change detected",$(this).val());
  var page="";
  if($(this).attr('id')=="crm_selection_set")
  {
     page="_set";
  }
  var c=$(this).val();
  switch (c)
  {
    case 'nil':
          show_crm("manual_script"+page);
          break;
    case '1':
          show_crm("crm_1"+page);
          break;
    case '2':
          show_crm("crm_2"+page);
          break;
    case '3':
          show_crm("crm_3"+page);
          break;
    case '4':
          show_crm("crm_4"+page);
          break;
    case '5':
          show_crm("crm_5"+page);
          break;
    default:
          show_crm("nil");
          break;
  }
}

function show_crm(id)
{
  //console.log("change option for ",id);
  var crms=["manual_script","manual_script_set","crm_1","crm_1_set","crm_2","crm_2_set","crm_3","crm_3_set","crm_4","crm_4_set","crm_5","crm_5_set"];
  crms.forEach( function(element, index) {
      if(element==id)
      {
        document.getElementById(element).style.display = 'inline';
      }
      else {
        document.getElementById(element).style.display = 'none';
      }
  });
  if(id=="manual_script" || id=="manual_script_set")
  {
    var x=id.replace("manual_script","domain_manual");
    document.getElementById(x).style.display = 'inline';
  }
  else
  {
    document.getElementById("domain_manual").style.display = 'none';
    document.getElementById("domain_manual_set").style.display = 'none';
  }
}

function clear_input_in_settingsUI()
{
  document.getElementById("sid").value="";
  document.getElementById("token").value="";
  document.getElementById("client_name").value="";
  document.getElementById("api_key_1").value="";
  document.getElementById("api_key_1_set").value="";
  document.getElementById("agile_domain").value="";
  document.getElementById("agile_domain_set").value="";
  document.getElementById("api_key_2_usr").value="";
  document.getElementById("api_key_2_usr_set").value="";
  document.getElementById("api_key_2_key").value="";
  document.getElementById("api_key_2_key_set").value="";
  document.getElementById("api_key_3").value="";
  document.getElementById("api_key_3_set").value="";
  document.getElementById("zoho_usr").value="";
  document.getElementById("zoho_usr_set").value="";
  document.getElementById("zoho_pwd").value="";
  document.getElementById("zoho_pwd_set").value="";
  document.getElementById("callback_url").value="";
  document.getElementById("callback_url_set").value="";
  document.getElementById("domain_url").value="";
  document.getElementById("domain_url_set").value="";
}

 function get_zoho_token(crm_type,domain_url,checked,usr,pwd,update_or_set)
 {
    var http=new XMLHttpRequest();
    var url = "https://accounts.zoho.com/apiauthtoken/nb/create?SCOPE=ZohoCRM/crmapi&EMAIL_ID="+usr+"&PASSWORD="+pwd+"&DISPLAY_NAME=Phone_dialer";
    http.open("GET", url, true);
    http.onreadystatechange = function() {//Call a function when the state changes.
            if(http.readyState == 4 && http.status == 200) {
                var response=http.response;
                console.log("response from zoho crm",response);
                var key=response.split("AUTHTOKEN=");
                console.log("key part:",key[1]);
                if(key[1])
                {
                  var auth_key_arr =key[1].replace( /\n/g, " " ).split( " " );
                  //console.log("auth key :",auth_key_arr);
                  var auth_key=auth_key_arr[0].trim();
                  console.log("auth_key",auth_key);  
                  localStorage["zoho_usr"]=usr;
                  localStorage["zoho_pwd"]=pwd;
                  chrome.extension.getBackgroundPage().update_CRM(crm_type,domain_url,checked,auth_key,null,null,null,update_or_set);
                  clear_input_in_settingsUI();
                }
                else
                {
                  var respns=response.split("CAUSE=");
                  if(respns[1])
                  {
                      var cause=respns[1].replace( /\n/g, " " ).split( " " );
                      if(cause[0]=="EXCEEDED_MAXIMUM_ALLOWED_AUTHTOKENS")
                        change_log("log_div","log","token limit reached for ZOHO.");
                      else
                        change_log("log_div","log","ZOHO:"+cause[0].trim());
                  }
                  else
                      change_log("log_div","log","enter valid details for ZOHO.");
                }
                return;
        }
    }
    http.send();
 }

 $(".pwd_show_hide").on('click',function(){
    console.log("pwd_show_hide click",$(this).attr('id'));
   var id=$(this).attr('id');
   switch (id) {
     case 'show_zoho_pwd':
     case 'show_zoho_pwd_set':
          document.getElementById("hide_zoho_pwd").style.display = 'inline';
          document.getElementById("hide_zoho_pwd_set").style.display = 'inline';
          document.getElementById("show_zoho_pwd").style.display = 'none';
          document.getElementById("show_zoho_pwd_set").style.display = 'none';
          document.getElementById("zoho_pwd").type="text";
          document.getElementById("zoho_pwd_set").type="text";
       break;
     case 'hide_zoho_pwd':
     case 'hide_zoho_pwd_set':
          document.getElementById("hide_zoho_pwd").style.display = 'none';
          document.getElementById("hide_zoho_pwd_set").style.display = 'none';
          document.getElementById("show_zoho_pwd").style.display = 'inline';
          document.getElementById("show_zoho_pwd_set").style.display = 'inline';
          document.getElementById("zoho_pwd").type="password";
          document.getElementById("zoho_pwd_set").type="password";
       break;
     case 'show_twilio_token':
          document.getElementById("show_twilio_token").style.display = 'none';
          document.getElementById("hide_twilio_token").style.display = 'inline';
          document.getElementById("token").type="text";
       break;
     case 'hide_twilio_token':
          document.getElementById("hide_twilio_token").style.display = 'none';
          document.getElementById("show_twilio_token").style.display = 'inline';
          document.getElementById("token").type="password";
       break;

     default:
       break;
   }
 });

 function request_permission()
 {
  chrome.extension.getBackgroundPage().get_microphone_permission();
  window.open('permission.html');
 }

 /*function register_email(){
  var usr_email_missing= document.getElementById("usr_email_missing").value;
  usr_email_missing=usr_email_missing.trim();
  if(!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(usr_email_missing) || usr_email_missing=="")
  {
    change_log("log_div","log","Provide a valid email.");
  }
  else{
      chrome.extension.getBackgroundPage().register_email(usr_email_missing);
  }

}*/
});

// ENHANCEMENT 
// AUTO CALL SWITCH TOGGLE
function enableDisableAutoCall(){
    var checked = localStorage.getItem('checked_auto_call101') === "true"; 
  $('#auto-call').prop('checked', checked);
  // $('#auto-call-body').toggle(checked);

  $('#auto-call').change(function () {
    var isChecked = $(this).is(':checked');
    localStorage.setItem('checked_auto_call101', isChecked);
    chrome.storage.local.set({'switch_status': localStorage.getItem('checked_auto_call101')});
  });
}
enableDisableAutoCall();