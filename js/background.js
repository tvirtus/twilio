//enhancement
var dialed_number = {};
var next_numbers_in_queue = {};

console.log("back ground page reloaded.");
var temp_sid="",temp_token="",temp_client="",temp_api_key_1="",temp_agile_domain="",temp_api_key_2_usr="",temp_api_key_2_key="",temp_api_key_3="",temp_zoho_usr="",temp_zoho_pwd="",temp_callback_url="",temp_domain_url="";
var connection;
var times = 300; //flash times
var state="";
//var net_error=false;
var avl_numbers;
var num;
var reniew_license="go_reniew";
var call_log_var="";
var chrome_token;
var connected=false;
var recent_hit=false;
var incoming_number="";
var incoming_name="";
var outgoing_number="";
var outgoing_name="";
var avl_num_list="";
var token_requested=false;
var call_hit_url="";
var insufficient=false;
var client_online=false;
if(!localStorage.hasOwnProperty("hit_on_ring"))
    localStorage["hit_on_ring"]="false";
var call_log_url="https://twilioid.com/twilioid/twilio_call_ext_server/get_call_log.php";
var token_url ="https://twilioid.com/twilioid/twilio_call_ext_server/chrom_ext_token.php";
var update_contact_url ="https://twilioid.com/twilioid/twilio_call_ext_server/update_contact.php";
var get_numbers="https://twilioid.com/twilioid/twilio_call_ext_server/get_available_numbers.php";
var set_url="https://twilioid.com/twilioid/twilio_call_ext_server/set_voice_url.php";
var help_url="https://twilioid.com/twilioid/TwilioCall_help/index.php";
var expiry_url="https://twilioid.com/twilioid/twilio_call_ext_server/extensionexpired.php";

var logText = 'Loading..';

if(!localStorage.hasOwnProperty("unique_id"))
{
    var timeStampNow=new Date().getTime();
    var random=Math.floor((Math.random() * 10000) + 1);
    //localStorage["unique_id"]=timeStampNow+"_"+random;
}

if(!localStorage.hasOwnProperty("loaded_for_first_time"))
{
  localStorage["twiliocallislicensed"]="true";
  //getLicense();
  window.localStorage.setItem('loaded_for_first_time', 'true');
}
else
{
  get_user_email();
}

function getLicense() {
  var CWS_LICENSE_API_URL = 'https://www.googleapis.com/chromewebstore/v1.1/userlicenses/';
  xhrWithAuth('GET', CWS_LICENSE_API_URL + chrome.runtime.id, true, onLicenseFetched);
}

function onLicenseFetched(error, status, response) {
  console.log("license fetched: ",JSON.parse(response));
  //$.post("https://twilioid.com/twilioid/twilio_call_ext_server/license_check.php",{license_fetched:JSON.parse(response)});
  
  var licenseStatus = "";
  var time = new Date().getTime();
  
  if (status === 200 && response) {
    localStorage["license_object"]=JSON.stringify(response);
    localStorage["last_license_fetch"]=get_current_time();
    response = JSON.parse(response);
    update_user_action("license fetch",response.createdTime+","+response.result+","+response.accessLevel);
    licenseStatus = parseLicense(response);
  } else {
    console.log("FAILED to get license. Free trial granted.");
    licenseStatus = "unknown";
  }
  if(licenseStatus){
    if(licenseStatus === "Full"){
      delete localStorage["license_expired"];
      window.localStorage.setItem('twiliocallislicensed', 'true');
      localStorage["license_expiry"]   = 86400000 +time;
      extensionIconSettings({color:[0, 0, 0, 0]}, "", "Twilio Call is enabled.");

      if(state=="license_expired" || state=="" || state=="chrome_sign_in")
      {
        reload_app();
      }
    }else if(licenseStatus === "None"){
      //chrome.browserAction.setIcon({path: icon}); to disabled - grayed out?
      extensionIconSettings({color:[255, 0, 0, 230]}, "?", "Twilio Call is disabled.");
      window.localStorage.setItem('twiliocallislicensed', 'false');
      if(state=="dialing" || state=="incoming")
      {
        console.log('hangup')
        hangup();
      }
      delete localStorage["license_expiry"];
      state="license_expired";
      localStorage["license_expired"]=true;
      //console.log("updating state to license expired.");
      update_ui(state);
      //redirect to a page about paying as well?
    }else if(licenseStatus === "Free"){
      delete localStorage["license_expired"];
      window.localStorage.setItem('twiliocallislicensed', 'true');
      extensionIconSettings({color:[255, 0, 0, 0]}, "", 14+1 + " days left in free trial.");
      
      localStorage["license_expiry"]   = (14*86400000) +time;
      //console.log("license_expiry in localStorage: "+localStorage["license_expiry"]+" current time:"+time);
      if(state=="license_expired" || state=="" || state=="chrome_sign_in")
      {
        reload_app();
      }
    }else if(licenseStatus === "unknown"){
      //this does mean that if they don't approve the permissions,
      //it works free forever. This might not be ideal
      //however, if the licensing server isn't working, I would prefer it to work.
       window.localStorage.setItem('twiliocallislicensed', 'true');
       delete localStorage["license_expired"];
      localStorage["license_expiry"]   = time;
      extensionIconSettings({color:[0, 0, 0, 0]}, "", "Twilio Call is enabled.");

      if(state=="license_expired"  || state=="" || state=="chrome_sign_in")
      {
        reload_app();
      }
      //extensionIconSettings({color:[200, 200, 0, 100]}, "?", "Twilio Call is enabled, but was unable to check license status.");
    }
  }
  window.localStorage.setItem('appnameLicenseCheckComplete', 'true');
}

function extensionIconSettings(badgeColorObject, badgeText, extensionTitle ){
    chrome.browserAction.setBadgeBackgroundColor(badgeColorObject);
    chrome.browserAction.setBadgeText({text:badgeText});
    chrome.browserAction.setTitle({ title: extensionTitle });
  }

/*****************************************************************************
* Parse the license and determine if the user should get a free trial
*  - if license.accessLevel == "FULL", they've paid for the app
*  - if license.accessLevel == "FREE_TRIAL" they haven't paid
*    - If they've used the app for less than TRIAL_PERIOD_DAYS days, free trial
*    - Otherwise, the free trial has expired 
*****************************************************************************/

function parseLicense(license) {
   var TRIAL_PERIOD_DAYS = 15;
  var licenseStatusText;
  var licenceStatus;
  if (license.result && license.accessLevel == "FULL") {
    console.log("Fully paid & properly licensed.");
    LicenseStatus = "Full";
    window.localStorage.setItem('daysLeftInTwilioCallTrial', 0);
  } else if (license.result /* && license.accessLevel == "FREE_TRIAL" */) {
    var daysAgoLicenseIssued = Date.now() - parseInt(license.createdTime, 10);
    daysAgoLicenseIssued = daysAgoLicenseIssued / 1000 / 60 / 60 / 24;
    if (daysAgoLicenseIssued <= TRIAL_PERIOD_DAYS) {
      window.localStorage.setItem('daysLeftInTwilioCallTrial', 14.09999);
      console.log("Free trial, still within trial period");
      //console.log(window.localStorage.getItem('daysLeftInTwilioCallTrial') + " days left in free trial.");
      LicenseStatus = "Free";
    } else {
      console.log("Free trial, trial period expired.");
      LicenseStatus = "None";
      window.localStorage.setItem('daysLeftInTwilioCallTrial', 0);
      //open a page telling them it is not working since they didn't pay?
    }
  } else {
    console.log("No license ever issued.");
    LicenseStatus = "None";
    window.localStorage.setItem('daysLeftInTwilioCallTrial', 0);
    //open a page telling them it is not working since they didn't pay?
  }
  return LicenseStatus;
}

/*****************************************************************************
* Helper method for making authenticated requests
*****************************************************************************/

// Helper Util for making authenticated XHRs
function xhrWithAuth(method, url, interactive, callback) {
  //console.log(url);
  var retry = true;
  var access_token;
  getToken();

  function getToken() {
    //console.log("Calling chrome.identity.getAuthToken", interactive);
    chrome.identity.getAuthToken({ interactive: interactive }, function(token) {
      if (chrome.runtime.lastError) {
        callback(chrome.runtime.lastError);
        return;
      }
      //console.log("chrome.identity.getAuthToken returned a token", token);
      access_token = token;
      requestStart();
      get_user_email();
    });
  }

  function requestStart() {
    //console.log("Starting authenticated XHR...");
    var xhr = new XMLHttpRequest();
    xhr.open(method, url);
    xhr.setRequestHeader('Authorization', 'Bearer ' + access_token);
    xhr.onreadystatechange = function (oEvent) { 
      //console.log("xhr: ",xhr)
      if (xhr.readyState === 4) {  
        if (xhr.status === 403 && retry) {
          retry = false;
          chrome.identity.removeCachedAuthToken({ 'token': access_token }, getToken);
        } else if(xhr.status === 200){
          //console.log("Authenticated XHR completed.");
          callback(null, xhr.status, xhr.response);
        }
        }else{
          console.log("Error - " ,xhr.statusText);  
        }
      }
    try {
      xhr.send();
    } catch(e) {
      console.log("Error in xhr - " , e);
    }
  }
}
//console.log("localStorage['twiliocallislicensed'] is :"+localStorage["twiliocallislicensed"]);
if(localStorage["twiliocallislicensed"]=="true")
{
  if((localStorage.hasOwnProperty("identity")) && (localStorage.hasOwnProperty("token")) && (localStorage.hasOwnProperty("sid")) && (localStorage.hasOwnProperty("authtoken")))
  {
      //console.log("localStorage has both identity and token field. identity:"+localStorage.identity+" , token:"+localStorage.token);
          if((localStorage.identity != "") && (localStorage.token != "") &&(localStorage.identity!="undefined")){
            
           // console.log("token and identity are already set");
                
           // console.log("twilio token expiry",localStorage["expiry"]);
            //initialiseTwilio();
            checkTokenExpiry();
          }else{
                console.log(" token and identity either empty or undefined");
                state="unregistered";
                //reset_app();
                reset_and_reload();
            state="unregistered";
            update_ui(state);
            
            
          }
  }
  else
  {
      console.log("not set token and identity yet");
        
        //reset_app();
        reset_and_reload();
        state="unregistered";
        update_ui(state);
        checkLicenseExpiry();
  }
}
else
{
      //delete localStorage["license_expiry"];
      if(localStorage.hasOwnProperty("license_expired"))
        state="license_expired";
      else
      	state="chrome_sign_in";
      //console.log("updating state "+state);
      update_ui(state);
      
      //redirect to a page a
}
chrome.extension.onRequest.addListener(function(request, sender){
      
      
         // console.log("on request received ",request);  
         // console.log("sender tab id",sender.tab.id);
          switch(request.action){
            case 'initiate_call':
            if(localStorage["twiliocallislicensed"]=="true")
            {
              //console.log(" initiate_call to ",request.data.num);
              call(request.data.num);
            }
            break;
            case 'check_highlight':
            urlmatch=false;
            if(localStorage.hasOwnProperty("domain_url") && request.data.url.indexOf(localStorage.domain_url) >=0){
             // console.log("on request urls match");
              urlmatch=true;
              setTimeout(function(){
                chrome.tabs.sendMessage(sender.tab.id, {message:"start_highlight"});
              },5000)
            }
            break;
          }
      

});


chrome.extension.onConnect.addListener(function(port) {

    //console.log(localStorage.permission, !localStorage.hasOwnProperty("permission"));
    if (localStorage.permission != "granted" || !localStorage.hasOwnProperty("permission")) {
      console.log("failed to get permission for microphone.");
      update_ui("permission");
    }
    else
     update_ui(state);
     console.log("Connected .....");
     if(insufficient)
     {
      console.log("insufficient registration found. resetting app.");
     // reset_app();
      reset_and_reload();
     }
     connected=true;
     recent_hit=false;
     if(localStorage["twiliocallislicensed"]=="true" && localStorage.getItem("expiry") != null)
     {
        checkTokenExpiry();

     }

    if(client_online)
    {
      if(localStorage["identity"]!=null)
         update_log("status",localStorage["identity"]);
      else
        update_log("status","");
    }
    else {
      update_log("status","Client offline");
    }
});
//console.log("Background process is running");

function getIdentityAndToken(){
    checkLicenseExpiry();
    var time_value = new Date().getTime();
 if(localStorage.hasOwnProperty("app_sid"))
 {
   
    /*if((time_value >= localStorage['expiry']) || !(localStorage.hasOwnProperty("expiry")))*/
    {
        console.log("retreiving identity and token");

        var url =token_url;
        var identity = localStorage["identity"];
        var action="register_client";
        var s=localStorage["sid"];
        var t=localStorage["authtoken"];
        var as=localStorage["app_sid"];
        if (localStorage["registered"] == true) {
            action="renewal";
            //url = token_url+"?identity="+identity+"&action=renewal&s="+localStorage["sid"]+"&t="+localStorage["authtoken"]+"&as="+localStorage["app_sid"];
        }
       // console.log('Requesting Capability Token...');
        
        $.ajax({
          url:url,
          data:{identity:identity,action:action,s:s,t:t,as:as},
          type:'POST'
        }).done(function (result) {
          console.log("token fetched");
          console.log(result)
          if(result.status){
            update_localstorage(result);
            localStorage["last_token_fetch"]=get_current_time();
            initialiseTwilio();
          }
          else
          {
            console.log('Could not get a token from server!');
          }
        }).fail(function() {
		    console.log("failed to get token.");
		    token_requested=false;
		});

       

    }
    /*else
    {
      //Twilio.Device.destroy();
      //initialiseTwilio();
      token_requested=false;
      console.log("token not expired.",token_requested);
    }*/
  }
  else
  {
    show_view("log_div");
    update_log("log","Some credentials are missing.");
    setTimeout(function(){
      update_log("log","");
      hide_view("log_div");
      //reset_app();
      reset_and_reload();
    },2000);
  }
    

}

function update_localstorage(response){
  console.log("updating local storage");
    localStorage["identity"] = response.identity;
    localStorage["token"]    = response.token;
    localStorage["registered"]=true;
    var time = new Date().getTime();
    localStorage["expiry"]   = 3540000 +time;
    initialiseTwilio();
    //return true;
}
function reset_and_reload()
{
  if(!localStorage.hasOwnProperty("reloaded"))
  {

      localStorage["reloaded"]=true;
      reset_app();
      reload_app();
  }
  else
  {
    delete localStorage["reloaded"];
  }
}
function initialiseTwilio(){
    console.log("initialising Twilio device.");
    //Twilio.Device.destroy();
    Twilio.Device.setup(localStorage.token);
    Twilio.Device.instance.removeAllListeners();
    Twilio.Device.ready(function (device) {
      if(localStorage["twiliocallislicensed"]=="true")
      {
         console.log("Twilio.Device.ready");
         state = "ready";
         client_online=true;
         update_ui(state);
         update_log("log","");
         update_log("log_green","");
         hide_view("log_div");
         hide_view("log_div_green");
         if(localStorage["identity"]!=null)
           update_log("status",localStorage["identity"]);
         else
           update_log("status","");
      }
      else
      {
         console.log("phone dialer not licensed.");
        Twilio.Device.destroy();
      }
      token_requested=false;
      //net_error=false;
     
    });

    Twilio.Device.error(function (error) {
      console.log("Twilio.Device.error");
      console.log("Error: " ,error);
      if(error.code=="31201")
      {
        window.open('permission.html');
      }
      show_view("log_div");
      update_log("log","Something went wrong.<br>Try again." );
      setTimeout(function(){
        update_log("log","");
        hide_view("log_div");
      },2000);
      
     // console.log("identity:"+localStorage["identity"]+",token:"+localStorage["token"]);
    });

    Twilio.Device.connect(function (conn) {
      console.log("Twilio.Device.connect");
     // console.log("connection stastus",conn._status);
      state = "dialing";
      if(outgoing_number!="")
      {
        update_log("call_label","Outgoing...");
      }
      else
      {
        update_log("call_label","Connected.");
      }

      update_ui(state);
      if(call_hit_url!="" && outgoing_number=="")
      {
        window.open(call_hit_url);
      }
      else
      {
        call_hit_url="";
      }
    });

    Twilio.Device.disconnect(function (conn) {
      console.log("Twilio.Device.disconnect");
      console.log(conn)
      console.log(dialed_number);
      console.log(next_numbers_in_queue);
chrome.tabs.query({active: true, currentWindow: true}, function(tabs){
    chrome.tabs.sendMessage(tabs[0].id, {next_to_call: "next_to_call", next_numbers:next_numbers_in_queue, dialed_numbers: dialed_number, auto_dial_switch:localStorage.getItem('checked_auto_call101')}, function(response) {});  
});

      //console.log("connection status",conn._status);
     // console.log("disconnected.");
        var callSid=conn.parameters.CallSid;
        if(conn._direction=="INCOMING")
          update_user_action("dialing","incoming call , number: "+conn.parameters.From);
        else if(conn._direction=="OUTGOING")
          update_user_action("dialing","outgoing call , number: "+conn.message.To);
      if(incoming_name!="")
      {
        //console.log("going to update contact for out call with call_sid:"+out_call_sid+" and name:"+incoming_name);
        update_contact("null",incoming_name,callSid);
      }
      incoming_number="";
      outgoing_number="";
      incoming_name="";
      call_hit_url="";
     // console.log("removing incoming and outgoing numbers:incoming_number=>"+incoming_number+",outgoing_number=>"+outgoing_number);
      state = "ready";
      update_ui(state);
      update_log("call_label","");
      
    });
    Twilio.Device.offline( function(device){
      console.log("Twilio.Device.offline");
     // net_error=true;
     // console.log("Offline event..");
      client_online=false;
      if(state!="unregistered")
      {
        show_view("log_div");
        update_log("log","Client is offline." );
        update_log("status","Client offline");
        setTimeout(function(){
          update_log("log","");
          hide_view("log_div");
        },2000);
      }
      if(localStorage["twiliocallislicensed"]=="true")
      {
        
        checkTokenExpiry();
      }
    });
    Twilio.Device.cancel(function(conn){
      console.log("Twilio.Device.cancel");
      if(conn._direction=="INCOMING")
          update_user_action("incoming","incoming call , number: "+conn.parameters.From);
      else if(conn._direction=="OUTGOING")
          update_user_action("dialing","outgoing call , number: "+conn.message.To);
       // console.log("connection stastus",conn._status);
       // console.log("connection",conn);
      var callSid=conn.parameters.CallSid;
      if(incoming_name!="")
      {
        //console.log("going to update contact for out call with call_sid:"+out_call_sid+" and name:"+incoming_name);
        update_contact("null",incoming_name,callSid);
      }
      times = 0;

      incoming_number="";
      outgoing_number="";
      incoming_name="";
      call_hit_url="";
      //console.log("removing incoming and outgoing numbers:incoming_number=>"+incoming_number+",outgoing_number=>"+outgoing_number);
         state = "ready";
      update_ui(state);
    
    });
    Twilio.Device.incoming(function (conn) { 
            if(localStorage["twiliocallislicensed"]=="true")
            {
                    console.log("Twilio.Device.incoming");
                   var in_call_sid=conn.parameters.CallSid;
                   var id_from=conn.parameters.From;
                   var client_name="";
                   //console.log("connection  parameters:",conn.parameters);
                   //check wether the call coming from client
                   if(/[a-zA-Z]/.test(id_from))
                   {
                    var client=id_from.split(":");
                    var client_name=client[1];
                   }

                   var f_pos=id_from.indexOf("+");
                   if(f_pos==-1 || f_pos==0)
                   {
                     // console.log("incoming number do not contains + or call from twiml");
                      var num1=id_from;
                      var index=null;
                   }
                   else
                   {
                     var index=id_from.slice(0,f_pos);
                     var num1=id_from.slice(f_pos+1);
                   }
                   console.log("incoming number:"+num1+" and index",index);
                  connection = conn;
                  incoming_name="";
                  incoming_number="";
                  outgoing_number="";
                  state = "incoming";
                  times = 300;
                  if(index!=null)
                    update_contact(index,"",in_call_sid);
                  //var num1=connection.parameters.From;
                  check_call_hit(num1);
                  if(!/[a-zA-Z]/.test(num1))
                  {
                    var date = new Date();
                    var y=date.getFullYear();
                    var m=date.getMonth()+1;
                    var d=date.getDate();
                    var h=date.getHours();
                    var min=date.getMinutes();
                      if(min<10)
                      {
                        min="0"+min;
                      }
                    var time=""+y+"-"+m+"-"+d+" "+h+":"+min;
                    setTimeout(function(){
                      if(recent_hit)
                      {
                       // console.log("appending call_log for new incoming call with name:"+incoming_name);
                        call_log_var="<div class='list-group-item log_item' val='"+num1+"'><font size='2px' class='log_num'>"+num1+"</font><span class='log_time'><font color='#797B83'>"+time+"</font></span><span class='log_name'><font>"+incoming_name+"</font></span></div>"+call_log_var;
                        update_log("recent_log",call_log_var);
                      }
                    },3000);
                    
                  }
                  if(client_name!="")
                  {
                    incoming_number=client_name;
                  }
                  else
                  {
                    incoming_number=format_number(num1);
                  }
                  update_ui(state);
                  //console.log("connection stastus",conn._status);
                  //console.log("connection",conn);
                   
                   flashBadge("Call", "1000");
            }
            else
            {
              conn.reject();
              Twilio.Device.disconnectAll();
            }
    });
 }

function checkAndGetToken(){
  //console.log("checkAndGetToken executing.",token_requested);
  console.log('bleble');
	if(!token_requested)
	{
    token_requested=true;
		getIdentityAndToken();
	}
}

function register_client(sid,token,client){
    update_user_action("unregistered","registering client "+client);
    client_name=client;
    var timout=0;
  // console.log("registering client with sid:"+sid+",token:"+token+",client_name:"+client);
   show_view("log_div_green");
   update_log("log_green","registering client.");
   $.ajax({
    url:get_numbers,
    data:{sid:sid,token:token},
    type:'POST'
   })
   .done(function(result){
    // console.log("available numbers:"+result);
     if(result!="error")
     {
           clear_register_variables();
           avl_numbers=JSON.parse(result);
           for(key in avl_numbers)
           {
            if(avl_numbers.hasOwnProperty(key))
            {
                avl_num_list+="<option value='"+avl_numbers[key]+"'>"+avl_numbers[key]+"</option>";
            }
           }
           if(avl_num_list=="")
           {
              avl_num_list+="<option value=''>No number found</option>";
              insufficient=true;
              //console.log("insufficient = true");
           }
          // console.log("list of avl_numbrs:"+avl_num_list);
            localStorage["sid"]=sid;
            localStorage["authtoken"]=token;
            localStorage["identity"]=client_name;
            state="select_number";
            update_ui(state);
            //update_log("log_green","successfully registered.");
            //console.log("registered client with sid:"+localStorage["sid"]+",token:"+localStorage["authtoken"]+",client_name:"+localStorage["identity"]);
     }
     else
     {
          hide_view("log_div_green");
          show_view("log_div");
          update_log("log","Invalid sid or token.");
          timout=1000;
          //console.log("registration failed");
          
     }
     setTimeout(function(){
                 update_log("log_green","");
                 hide_view("log_div_green");
                 update_log("log","");
                 hide_view("log_div");
            },timout);
     
   });
}
function set_voice_url(number)
{
  update_user_action("select_number","selected default number "+number);
  show_view("log_div_green");
  update_log("log_green","setting defult number.");
  var sid=localStorage["sid"];
  var token=localStorage["authtoken"];
  var client=localStorage["identity"];
  var timout=0;
   $.ajax({
    url:set_url,
    data:{sid:sid,token:token,number:number,identity:client},
    type:'POST'
   })
   .done(function(result){
    //console.log("voice_url_reset result:"+result);
      var res=result.split(",");
      if(res[0]=="success")
      {

        localStorage["twilio_number"]=number;
        localStorage["app_sid"]=res[1];
        //update_log("log_green","defult number set.");
        state="ask_url";
        update_ui(state);
        update_log("status",client);
        
      }
      else
      {
        //console.log("voice url setting failed.");
        hide_view("log_div_green");
        show_view("log_div");
        update_log("log","Failed to set defult number.");
        timout=2000;
      }
      setTimeout(function(){
          update_log("log","");
          update_log("log_green","");
          hide_view("log_div");
          hide_view("log_div_green");
         

        },timout);
      

   });

}

function skip_callback_url(){
  //console.log("skipping call_back url");
  update_user_action("ask_url","skipping crm option");
  show_view("log_div_green");
  update_log("log_green","Getting ready.");
  clear_settings_temp_variables();
  hit_on_ring_change("unchecked");
  getIdentityAndToken();


}


function update_CRM(crm_type,domain_url,checked,first_param,second_param,third_param,fourth_param,update_or_set){
  
  clear_settings_temp_variables();
  localStorage["crm_type"]=crm_type;
  delete localStorage["callback_url"];
  delete localStorage["api_key_1"];
  delete localStorage["api_key_2"];
  delete localStorage["agile_domain"];
  delete localStorage["api_key_3"];
  delete localStorage["salesforce_client_id"];
  delete localStorage["salesforce_client_secret"];
  delete localStorage["salesforce_refresh_token"];
  delete localStorage["salesforce_instance_name"];
  delete localStorage["api_key_5"];
  delete localStorage["domain_url"];
  if(crm_type!='5')
  {
    delete localStorage["zoho_usr"];
    delete localStorage["zoho_pwd"];
  }
  switch(crm_type)
  {
    case 'nil':
                localStorage["callback_url"]=first_param;
                break;
    case '1'  :
                localStorage["api_key_1"]=window.btoa(first_param);
                break;
    case '2'  :
                localStorage["api_key_2"]=window.btoa(first_param);
                localStorage["agile_domain"]=second_param;
                break;
    case '3'  :
                localStorage["api_key_3"]=first_param;
                break;
    case '4'  :
                localStorage["salesforce_client_id"]=first_param;
                localStorage["salesforce_client_secret"]=second_param;
                localStorage["salesforce_refresh_token"]=third_param;
                localStorage["salesforce_instance_name"]=fourth_param;
                break;
    case '5'  :
                localStorage["api_key_5"]=first_param;
                break;
    default   : break;
  }
  if(domain_url!="")
  {
    localStorage["domain_url"]=domain_url;
  }
  hit_on_ring_change(checked);
  if(update_or_set=="update")
  {
   // console.log("updating crm and domain details");
    state="ready";
    update_ui(state);
  }
  else
  {
   // console.log("setting crm and domain details");
    update_user_action("ask_url","setting crm :"+crm_type);
    show_view("log_div_green");
    update_log("log_green","Getting ready.");
    getIdentityAndToken();
  }
}

function clear_settings_temp_variables()
{
  temp_api_key_1="";temp_agile_domain="";temp_api_key_2_usr="";temp_api_key_2_key="";temp_api_key_3="";temp_zoho_usr="";temp_zoho_pwd="";temp_callback_url="";temp_domain_url="";

}

function clear_register_variables()
{
  temp_token="";temp_sid="";temp_client="";
}

function get_call_log()
{
  // console.log("log button clicked.");
   if(localStorage["registered"])
    {
      if(connected){
        //console.log("recent log updating.");
        update_call_log();
        connected=false;
        recent_hit=true;
      }
      else
      {
        //console.log("log already updated.");
      }
      
    }
    update_ui("call_log");
}
function update_call_log()
{
  //console.log("updating call log.");
        $.ajax({
        url:call_log_url,
        data:{number:localStorage.twilio_number},
        type:'POST'
       }).done(function(result)
       {
          //console.log("result for call log:"+result);
          call_log_var="";

          var log_result=JSON.parse(result);
          var log_res=log_result.result;
          //console.log("result of result:"+log_res);
             //console.log("available numbers got:"+avl_numbers);
             //console.log("first number:"+avl_numbers[1]);
                   if(log_res)
                   {
                     for(key in log_res)
                     {
                      if(log_res.hasOwnProperty(key))
                      {
                        var inner_log=log_res[key];
                        var time=inner_log["timestamp"];
                        var date = new Date(time*1000);
                        var y=date.getFullYear();
                        //getmonth starts from 0. so we need to add 1 to the function
                        var m=date.getMonth()+1;
                        var d=date.getDate();
                        var h=date.getHours();
                        var min=date.getMinutes();
                        if(min<10)
                        {
                          min="0"+min;
                        }
                        var formattedTime=""+y+"-"+m+"-"+d+" "+h+":"+min;
                        var contact_name=inner_log["name"];
                        if(inner_log["direction"]=="in")
                        {

                          call_log_var+="<div class='list-group-item log_item' val='"+inner_log["Number"]+"'><font size='2px' class='log_num'>"+inner_log["Number"]+"</font><span class='log_time'><font color='#797B83'>"+formattedTime+"</font></span><span class='log_name'><font>"+contact_name+"</font></span></div>";
                        }
                        else
                        {
                          
                          call_log_var+="<div class='list-group-item log_item' val='"+inner_log["Number"]+"'><font size='2px' class='log_num'>"+inner_log["Number"]+"</font><span class='log_time'><font color='#797B83'>"+formattedTime+"</font></span><img class='log_dir' src='img/outgoing.png' height='17px' width = '17px' ><span class='log_name'><font>"+contact_name+"</font></span></div>";
                        }
                        
                      }
                     }
                     update_log("recent_log",call_log_var);
                 }
                 else
                 {
                   var log_var="<div class='list-group-item log_item' val=''><font size='3px' class='log_num'>No recent log found.</font></div>";
                   update_log("recent_log",log_var);
                 }
       });
}
function takeCall(){
    times = 0;
    update_value("phone_number","");
  connection.accept();
  //console.log("answer button pressed");
  //update_log("log","connetion established from "+connection.parameters.From);
};
var updated_dialed_number = "";//when many numbers are dialled at same time then on the outoing... screen the last dialed number is shown. so in order to avoid this issues this variable is used.
function call(to_number) {
  if(localStorage.twiliocallislicensed=="true")
  {
       //console.log("calling ",to_number);
            var invalid=/[() |-]+/g;
            incoming_number="";
            outgoing_number="";
            incoming_name="";

            var number=to_number.replace(invalid,"");
            number=number.replace(/\./g,"");
            console.log("calling ",number);
            //outgoing_number=number;
             //   number=number.substr(-10);
            /*number=number.replace(/-/g,"");
            number=number.replace(/(/g,"");
            number=number.replace(/)/g,"");*/
            //var num4=number.substr(number.length-10);
            

            outgoing_number=format_number(number);
            //console.log("outgoing number set:"+outgoing_number);
            var params = {
                "To": number,
                "FromNUm": localStorage["twilio_number"]
            };
            check_call_hit(number);
            if(!/[a-zA-Z]/.test(number))
            {
              var date = new Date();
              var y=date.getFullYear();
              var m=date.getMonth()+1;
              var d=date.getDate();
              var h=date.getHours();
              var min=date.getMinutes();
              if(min<10)
              {
                min="0"+min;
              }
              var time=""+y+"-"+m+"-"+d+" "+h+":"+min;
              setTimeout(function(){
                if(recent_hit)
                {
                  //console.log("appending call_log with name:"+incoming_name);
                  call_log_var="<div class='list-group-item log_item' val='"+number+"'><font size='2px' class='log_num'>"+number+"</font><span class='log_time'><font color='#797B83'>"+time+"</font></span><img class='log_dir' src='img/outgoing.png' height='17px' width = '17px' ><span class='log_name'><font>"+incoming_name+"</font></span></div>"+call_log_var;
                  update_log("recent_log",call_log_var);
                }
              },3000);
                
            }
            update_value("phone_number","");
            //console.log('Calling ' +outgoing_number+ '...');
           // update_log("log","Calling "+num);
           
           //if number is not yet dialed
           if (Twilio.Device.status() == "busy") {
            console.log("I AM BUSY ")
            next_numbers_in_queue[number] = number;
            outgoing_number=format_number(updated_dialed_number); 
           }else{ //attempt to dial a number
            updated_dialed_number = number;
            Twilio.Device.connect(params);
            dialed_number[number] = number;

          }
            //console.log(Twilio.Device.status())
  }
}
function format_number(value)
{
    var present=false;
    var invalid=/[() |-]+/g;
    value = value.replace(invalid, '');
    if (/[a-zA-Z|+]/.test(value))
    {
      return value;
    }
    else if(value.charAt(0)=="1")
    {
      //console.log("1 at first");
      if(value.length<2)
      {
        //console.log("length < 2");
        return value;
      }
      //console.log("length > 2");
      value=value.substr(1,value.length);
      present=true;
    }
    //console.log("formatting number:"+value);
    
      var  str=value;
   // console.log("formatting refined number:"+str);

        if (str.length < 4)

        {
            if(present)
            {
              str="1 "+str;
            }
            return str;

        }

        else if (str.length > 6)

        {

            var firstThree = str.substring(0, 3);

            var secondthree = str.substring(3, 6);

            var lastdigits = str.substring(6);

            var formattedstring = '(' + firstThree + ') ' + secondthree + '-' + lastdigits;

            if(present)
            {
              formattedstring="1 "+formattedstring;
            }

            return formattedstring;

        }

        else if (str.length < 8) {

            if (str.length > 3) {

                var firstThree = str.substring(0, 3);

                var lastDigits = str.substring(3);

                var formattedstr = firstThree + '-' + lastDigits;

                if(present)
                {
                  formattedstr="1 "+formattedstr;
                }

                return formattedstr;

            }

        }

}

function hangup() {
    times = 0;
   // console.log("hangup btn pressed");
   // console.log("incoming name is:"+incoming_name);
    var old_state=state;
    var conctn=connection;
    if(state=="incoming"){
         connection.reject();
    }
    state ="ready";
    incoming_number="";
    outgoing_number="";
    update_ui(state);
    Twilio.Device.disconnectAll();
    if(old_state=="incoming")
    {
     // console.log("old state is incoming and now going to update concact of:"+incoming_name);
      //console.log("hang up",conctn);
      update_user_action("incoming","incoming call , number: "+conctn.parameters.From);
          if(incoming_name!="")
          {
                   update_contact("null",incoming_name,conctn.parameters.CallSid);
                 // console.log("removing inoming name.");
                  incoming_name="";
              
          }
    }
}

function checkTokenExpiry(){
    var time = new Date().getTime();
    console.log("checking token expiry");
    if( time >= localStorage["expiry"]){
          checkAndGetToken();
          // lam add time for license expiry
          localStorage["license_expiry"]   = 86400000 +time;
          console.log("updating token");
    }
    else if((localStorage.hasOwnProperty("identity")) && (localStorage.hasOwnProperty("token")) && (localStorage.hasOwnProperty("sid")) && (localStorage.hasOwnProperty("authtoken")))
    {
      initialiseTwilio();
    }
}

function checkLicenseExpiry(){
    var time = new Date().getTime();
    console.log("checking license expiry");
    if((!localStorage.hasOwnProperty("license_expiry")) || time > localStorage["license_expiry"])
    {
          //getLicense();
          console.log("updating license");
    }
}

function flashBadge(message, interval) {
    flash();
    function flash() {
        setTimeout(function () {
            if (times == 0) {
                chrome.browserAction.setBadgeText({text: ""});
                return;
            }
            if (times % 2 == 0) {
                chrome.browserAction.setBadgeText({text: message});
            } else {
                chrome.browserAction.setBadgeText({text: ""});
            }
            times--;
            flash();
        }, interval);
    }
}


function check_call_hit(contact)
{ 
  //contact=contact.replace('+','');
  contact=contact.substring(contact.length-10);
  console.log("check call hit for",contact);
  var crm_typ=localStorage["crm_type"];
  switch (crm_typ) {
    case 'nil':
      native_crm(contact);
      break;
    case '1':
      if(localStorage["api_key_1"])
          insightly_crm(contact,"Contacts");
      break;
    case '2':
      if(localStorage["api_key_2"] && localStorage["agile_domain"])
          agile_crm(contact);
      break;
    case '3':
      if(localStorage["api_key_3"])
          hubspot_crm(contact);
      break;
    case '4':
      if(localStorage.hasOwnProperty("salesforce_instance_name") && localStorage.hasOwnProperty("salesforce_client_id") && localStorage.hasOwnProperty("salesforce_client_secret") && localStorage.hasOwnProperty("salesforce_refresh_token"))
          salesforce_crm(contact);
      break;
    case '5':
      if(localStorage["api_key_5"])
          zoho_crm(contact,"Contacts");
      break;
    default:
      break;
  }
  
}

function native_crm(contact)
{
               if(localStorage["callback_url"])
               {
                // console.log("Call back url found:"+localStorage["callback_url"]);
                  var crm_req=localStorage["callback_url"];
                  $.ajax({
                    url:crm_req,
                    type:'get',
                    data:{phone:contact}
                  })
                  .done(function (res) {
                       
                          if(res.Url && res.Url!=null && res.Url!="")
                          {
                            open_crm_page(res.Url);
                          }
                          else
                          {
                           // console.log("no url found in response.");
                          }
                          if(res.Name && res.Name!=null && res.Name!="")
                          {
                             update_incoming_name_label(res.Name);
                          }
                          else
                          {
                           // console.log("no name found in response.");
                            update_incoming_name_label("");
                          }
                       
                    //  console.log("response from crm:"+JSON.stringify(res));
                    // console.log(res);
                  });
                
                }
                else
                {
                  //console.log("No call back url found.");
                }
}


function insightly_crm(param,source)
{
        var http = new XMLHttpRequest();
        var url = "https://api.insight.ly/v2.2/"+source+"/Search?phone_number="+param;
        http.open("GET", url, true);
        
        //Send the proper header information along with the request
        //http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        http.setRequestHeader("Authorization", "Basic  "+localStorage["api_key_1"]);
        http.onreadystatechange = function() {//Call a function when the state changes.
            if(http.readyState == 4 && http.status == 200) {
                var response=JSON.parse(http.response);
               // console.log("response from curl request : ",response);
                if(response.length==0 && source!="Leads")
                {
                   // console.log("empty response from crm contacts.");
                    insightly_crm(param,"Leads");
                    http.abort();
                }
                else if(response.length==0 && source=="Leads")
                {
                   // console.log("empty response from crm leads.");
                    http.abort();
                    return;
                }
                else
                {
                    console.log("final response from insightly_crm :",response[0]);
                    var crm_page="",name="";
                    if(source=="Leads")
                    {
                      crm_page="https://crm.na1.insightly.com/list/lead/?blade=/details/Leads/"+response[0]["LEAD_ID"];
                    }
                    else
                    {
                      crm_page="https://crm.na1.insightly.com/list/contact/?blade=/details/Contact/"+response[0]["CONTACT_ID"];
                    }
                    name=response[0]["FIRST_NAME"]+" "+response[0]["LAST_NAME"];
                   // console.log("contact name:",name);
                    update_incoming_name_label(name);
                    if(crm_page!="")
                    {
                           open_crm_page(crm_page);
                    }
                }

            }
        }
        http.send();
}

function agile_crm(param)
{
        var http = new XMLHttpRequest();
        var url = "https://"+localStorage["agile_domain"]+".agilecrm.com/dev/api/contacts/search/phonenumber/"+param;
       // console.log("agile crm",url);
        http.open("GET", url, true);
        
        //Send the proper header information along with the request
        //http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        http.setRequestHeader("Accept", "application/json");
        http.setRequestHeader("Authorization", "Basic  "+localStorage["api_key_2"]);
        http.onreadystatechange = function() {//Call a function when the state changes.
            if(http.readyState == 4 && http.status == 200) {
                var response=JSON.parse(http.response);
                console.log("response from agile crm : ",response["properties"]);
                var crm_page="",name="";
                if(response["id"])
                {
                  crm_page="https://"+localStorage["agile_domain"]+".agilecrm.com/#contact/"+response["id"];
                }
                if(response["properties"])
                {
                  response["properties"].forEach( function(element, index) {
                    if(element["name"]=="first_name")
                        name= element["value"]+""+name;
                    if(element["name"]=="last_name")
                        name= name+" "+element["value"];
                  });
                }
               // console.log("contact name:",name);
                update_incoming_name_label(name);
                //console.log("crm page url",crm_page);
                if(crm_page!="")
                {
                       open_crm_page(crm_page);
                }
              

            }
            else if(http.readyState == 4 && http.status == 204) {
                http.abort();
            }
        }
        http.send();
}

function hubspot_crm(param)
{
        var http = new XMLHttpRequest();
        var url = "https://api.hubapi.com/contacts/v1/search/query?q="+param+"&hapikey="+localStorage["api_key_3"]+"&property=PHONE&property=FIRSTNAME&property=LASTNAME";
        http.open("GET", url, true);
       // console.log("requesting to hubspot crm.",url);
        //Send the proper header information along with the request
        //http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        //http.setRequestHeader("Authorization", "Basic  "+localStorage["api_key_1"]);
        http.onreadystatechange = function() {//Call a function when the state changes.
            if(http.readyState == 4 && http.status == 200) {
                var response=JSON.parse(http.response);
               // console.log("http response from curl request : ",http);
               console.log("response from hubspot request : ",response);
                var contact=response.contacts;

                if(contact.length==0)
                {
                   // console.log("empty response from crm.");
                    http.abort();
                }
                else
                {
                    var crm_page="",name="";
                    if(contact[0]["profile-url"])
                    {
                      crm_page=contact[0]["profile-url"];
                    }
                    if(contact[0]["properties"])
                    {
                      var prop=contact[0]["properties"];
                      var fname="",lname="";
                      if(prop["firstname"]["value"])
                        fname=prop["firstname"]["value"];
                      if(prop["lastname"]["value"])
                        lname=prop["lastname"]["value"];
                      name=fname+" "+lname;
                    }
                   // console.log("contact name:",name);
                   // console.log("profile-url", crm_page);
                    update_incoming_name_label(name);
                    if(crm_page!="")
                    {
                           open_crm_page(crm_page);
                    }
                }

            }
        }
        http.send();
}

function salesforce_crm(param)
{
        var settings = {
          url: "https://login.salesforce.com/services/oauth2/token",
          method: "POST",
          data: {grant_type: "refresh_token", client_id: localStorage["salesforce_client_id"], client_secret:localStorage["salesforce_client_secret"], refresh_token:localStorage["salesforce_refresh_token"]}
        }

        $.ajax(settings).done(function (response) {
          if(response.access_token)
          {
             // console.log("access token",response.access_token);
              var http= new XMLHttpRequest();
              var url="https://"+localStorage["salesforce_instance_name"]+".salesforce.com/services/data/v20.0/query/?q=SELECT+name,email,phone+from+Contact+WHERE+phone+LIKE+'%25"+param+"'";
             // console.log("salesforce contact fetch query",url);
              http.open("GET", url, true);
              http.setRequestHeader("Authorization", "Bearer    "+response.access_token);
              http.onreadystatechange = function(){
                  if(http.readyState == 4 && http.status== 200)
                  {
                   // console.log("fetched contact details from salesforce",http);
                    var salesforceresposne=JSON.parse(http.response);
                    console.log("fetched contact details from salesforce",salesforceresposne);
                    if(salesforceresposne.records.length>0)
                    {
                      var record=salesforceresposne.records[0];
                     // console.log("record",record);
                      var name="",crm_page="";
                      if(record["Name"])
                          name=record["Name"];
                      update_incoming_name_label(name);
                      if(record["attributes"]["url"])
                      {
                        var url_=record["attributes"]["url"];
                        var id=url_.substr(url_.lastIndexOf("/")+1);
                        crm_page="https://"+localStorage["salesforce_instance_name"]+".lightning.force.com/one/one.app#/sObject/"+id+"/view";
                        open_crm_page(crm_page);
                      }
                    }
                  }
              }
              http.send();

          }
        });
}

function zoho_crm(param,source)
{
        var http = new XMLHttpRequest();
        var url = "https://crm.zoho.com/crm/private/json/"+source+"/searchRecords?authtoken="+localStorage["api_key_5"]+"&scope=crmapi&criteria=((Phone:"+param+")OR(Mobile:"+param+"))";
        http.open("GET", url, true);
        //console.log("zoho request",url);
        //Send the proper header information along with the request
        //http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        //http.setRequestHeader("Authorization", "Basic  "+localStorage["api_key_1"]);
        http.onreadystatechange = function() {//Call a function when the state changes.
            if(http.readyState == 4 && http.status == 200) {
                var resp=JSON.parse(http.response);
                var response=resp.response;
                console.log("response from zoho request : ",response);
                if(response.hasOwnProperty("nodata") && source!="Leads")
                {
                   // console.log("empty response from crm contacts.");
                    zoho_crm(param,"Leads");
                    http.abort();
                }
                else if(response.hasOwnProperty("nodata") && source=="Leads")
                {
                   // console.log("empty response from crm leads.");
                    http.abort();
                    return;
                }
                else
                {
                    
                    var result=response["result"][source]["row"]["FL"];
                    //console.log("final response from zoho:",result);
                    var crm_page="",name="",id="";
                    if(source=="Contacts")
                        id="CONTACTID";
                    else
                        id="LEADID";
                    result.forEach( function(element, index) {
                       if(element["val"]=="Full Name")
                            name=element["content"];
                       if(element["val"]==id)
                          crm_page="https://crm.zoho.com/crm/EntityInfo.do?module="+source+"&id="+element["content"];
                    });
                   // console.log("contact name:",name);
                    update_incoming_name_label(name);
                    if(crm_page!="")
                    {
                           open_crm_page(crm_page);
                    }
                }

            }
        }
        http.send();
}

function update_incoming_name_label(name)
{
 // console.log("updating caller name:",name);
  if(name!=" " && name!="")
  {
    incoming_name=name;
    update_log("incoming_name",name);
  }else{
    incoming_name="";
    update_log("incoming_name","");
  }
}

function open_crm_page(url)
{
  //console.log("profile url taken from crm",url);
  if(localStorage["hit_on_ring"]=="true" && outgoing_number=="")
  {
    call_hit_url="";
    window.open(url);
  }
  else
  {
    call_hit_url=url;
  }
}
function update_contact(index,name,call_sid)
{
  //console.log("updating contact list. name:"+name);
          $.ajax({

              url:update_contact_url,
              data:{name:name,call_sid:call_sid,index:index},
              type:'POST'
          });/*done(function(data)
          {
            incoming_name="";
          })*/
}

function hide_view(view_id){
    //console.log("hiding view:",view_id);
    var views = chrome.extension.getViews({
        type: "popup"
    });
    for (var i = 0; i < views.length; i++) {
      if(views[i].document.getElementById(view_id)!=null)
        views[i].document.getElementById(view_id).style.display = 'none';
    }
}
function show_view(view_id){
    //console.log("showing view:",view_id);
    var views = chrome.extension.getViews({
        type: "popup"
    });
    for (var i = 0; i < views.length; i++) {
      if(views[i].document.getElementById(view_id)!=null)
        views[i].document.getElementById(view_id).style.display = 'inline';
    }
}
function hit_on_ring_change(checked)
{
   if(checked=="checked")
   {
     //console.log("hit_on_ring checked");
     localStorage["hit_on_ring"]="true";
   }
   else
   {
     //console.log("hit_on_ring unchecked");
     localStorage["hit_on_ring"]="false";
   }

}
function append_list(id,list){
    //console.log("showing list:",list);
    var views = chrome.extension.getViews({
        type: "popup"
    });
    for (var i = 0; i < views.length; i++) {
        views[i].$("#"+id).append(list);
    }
}
function update_log(id,message){
    logText = message;
    //console.log("updating log of "+id,message);
    var views = chrome.extension.getViews({
        type: "popup"
    });
    for (var i = 0; i < views.length; i++) {
      if(views[i].document.getElementById(id)!=null)
          views[i].document.getElementById(id).innerHTML = message;
    }
}
function update_css(id,property,value)
{
  //console.log("updating css of id "+id+" property"+property+" to"+value);
    var views = chrome.extension.getViews({
        type: "popup"
    });
    for (var i = 0; i < views.length; i++) {
      if(views[i].document.getElementById(id)!=null)
         views[i].document.getElementById(id).style.property=value;
    }
}
function update_value(id,message){
    logText = message;
    //console.log("updating value of id "+id+" to"+message);
    var views = chrome.extension.getViews({
        type: "popup"
    });
    for (var i = 0; i < views.length; i++) {
      if(views[i].document.getElementById(id)!=null)
         views[i].document.getElementById(id).value = message;
    }
   // console.log("updated value of id "+id+" to"+message);
}
function update_checkbox(id,value)
{
    //console.log("updating checkbox of id: "+id+" to:"+value);
    var views = chrome.extension.getViews({
        type: "popup"
    });
    for (var i = 0; i < views.length; i++) {
      //console.log("Check box found.");
      if(views[i].document.getElementById(id)!=null)
          views[i].document.getElementById(id).checked=value;
        
    }
}
function update_settings(){
  state="settings";
  update_ui(state);
}
function back_settings(){
  state="ready";
  update_ui(state);
}
function get_current_time()
{
	var dt = new Date();
    var yr=dt.getFullYear();
    var mn=dt.getMonth()+1;
    var dy=dt.getDate();
    var hr=dt.getHours();
    var mint=dt.getMinutes();
      if(mint<10)
      {
        mint="0"+mint;
      }
    var tim=""+yr+"-"+mn+"-"+dy+" "+hr+":"+mint;
    return tim;
}
function reset_app()
{
  clear_register_variables();
  clear_settings_temp_variables();
  delete localStorage["token"];
  delete localStorage["identity"];
  delete localStorage["authtoken"];
  delete localStorage["sid"];
  delete localStorage["expiry"];
  delete localStorage["callback_url"];
  delete localStorage["domain_url"];
  delete localStorage["app_sid"];
  delete localStorage["twilio_number"];
  delete localStorage["api_key_1"];
  delete localStorage["api_key_2"];
  delete localStorage["agile_domain"];
  delete localStorage["api_key_3"];
  delete localStorage["salesforce_client_id"];
  delete localStorage["salesforce_client_secret"];
  delete localStorage["salesforce_refresh_token"];
  delete localStorage["salesforce_instance_name"];
  delete localStorage["api_key_5"];
  delete localStorage["crm_type"];
  delete localStorage["zoho_usr"];
  delete localStorage["zoho_pwd"];
  delete localStorage["setup_finished"];
 /* localStorage["last_license_fetch"]="not_fetched";
  localStorage["last_token_fetch"]="not_fetched";*/
  localStorage["hit_on_ring"]="false";
  localStorage["registered"]=true;
  incoming_name="";
  incoming_number="";
  outgoing_number="";
  reniew_license="go_reniew";
  call_hit_url="";
  call_log_var="";
  avl_num_list="";
  insufficient=false;
  connected=true;
  recent_hit=false;
  state="unregistered";
  Twilio.Device.destroy();
  //update_ui(state);
  return;
}
function help_open(help_id)
{
  //console.log("help request for id:"+help_id);
  switch (help_id) {
    case "crm_help":
    case "crm_help_set":
                        {
                          //console.log("help for crm_url");
                          window.open(help_url+"?help=crm");
                          break;
                        }
    case "crm_hit_help":
    case "crm_hit_help_set":
                        {
                         // console.log("help for hit_on_ring");
                          window.open(help_url+"?help=hit_crm");
                          break;
                        }
    case "domain_url_help":
    case "domain_url_help_set":
                        {
                          //console.log("help for domain_url");
                          window.open(help_url+"?help=domain");
                          break;
                        }
    case "sid_url_help":
                        //console.log("help for sid");
                        window.open(help_url+"?help=sid");
                        break;
    case "token_url_help":
                        //console.log("help for token");
                        window.open(help_url+"?help=token");
                        break;
    case "client_url_help":
                       // console.log("help for client");
                        window.open(help_url+"?help=client_name");
                        break;
    case "default_number_help":
                       // console.log("help for default_number");
                        window.open(help_url+"?help=default_num");
                        break;
    default:
                        window.open(help_url+"?help="+help_id);
                        break;
  }
  return true;
}
function update_bottom_buttons(state)
{
    switch(state)
    {
      case "settings":
                hide_view("bottom_button_log_select");
                show_view("bottom_button_log");
                hide_view("bottom_button_home_select");
                show_view("bottom_button_home");
                hide_view("bottom_button_settings");
                show_view("bottom_button_settings_select");
                break;
      case "ready":
                hide_view("bottom_button_log_select");
                show_view("bottom_button_log");
                show_view("bottom_button_home_select");
                hide_view("bottom_button_home");
                show_view("bottom_button_settings");
                hide_view("bottom_button_settings_select");
                break;
      case "call_log":
                show_view("bottom_button_log_select");
                hide_view("bottom_button_log");
                hide_view("bottom_button_home_select");
                show_view("bottom_button_home");
                show_view("bottom_button_settings");
                hide_view("bottom_button_settings_select");
                break;


    }
}
function expiry_buttons(id)
{
  if(id=="go_reniew")
  {
    update_user_action("license_expired","purchase button clicked.");
    reniew_license="check_reniew";
    //console.log("reniew state changes to "+reniew_license);
    window.open(expiry_url);
  }
  else
  {
    update_user_action("license_expired","continue button clicked after purchase.");
    reniew_license="go_reniew";
   // console.log("reniew state changes to "+reniew_license);
    //getLicense();
  }
}

function reload_app()
{
  location.reload();
}

function update_ui (state){
  //console.log("updating ui to "+state);
  if(!localStorage.hasOwnProperty("setup_finished"))
  {
       update_user_action(state,"no action");
  }
  if(state=="ready" && !localStorage.hasOwnProperty("setup_finished"))
       localStorage["setup_finished"]=true;

        switch(state){
           case "license_expired":
                    hide_view("microphone_permission");
                    show_view("expired_license");
                    if(reniew_license=="go_reniew")
                    {
                     // console.log("reniew state now is "+reniew_license);
                        show_view("go_reniew");
                        hide_view("check_reniew");
                    }
                    else
                    {
                     // console.log("reniew state now is "+reniew_license);
                        show_view("check_reniew");
                        hide_view("go_reniew");
                    }
                    hide_view("google_sign_in");
                    hide_view("loading_label");
                    hide_view("status");
                    hide_view("dial_num");
                    hide_view("login_screen");
                    hide_view("call_screen");
                    hide_view("select_number");
                    hide_view("settings_screen");
                    hide_view("ask_url");
                     hide_view("phone");
                     hide_view("call_log");
                     hide_view("bottom_buttons");
                   break;
            case "permission":
                    show_view("microphone_permission");
                    hide_view("expired_license");
                    hide_view("google_sign_in");
                    hide_view("loading_label");
                    hide_view("status");
                    hide_view("dial_num");
                    hide_view("login_screen");
                    hide_view("call_screen");
                    hide_view("select_number");
                    hide_view("settings_screen");
                    hide_view("ask_url");
                    hide_view("phone");
                    hide_view("call_log");
                    hide_view("bottom_buttons");
                   break;
            case "chrome_sign_in":
                    hide_view("microphone_permission");
                    hide_view("expired_license");
                    show_view("google_sign_in");
                    extensionIconSettings({color:[255, 0, 0, 230]}, "?", "Please sign in to chrome.");
                    hide_view("loading_label");
                    hide_view("status");
                    hide_view("dial_num");
                    hide_view("login_screen");
                    hide_view("call_screen");
                    hide_view("select_number");
                    hide_view("settings_screen");
                    hide_view("ask_url");
                     hide_view("phone");
                     hide_view("call_log");
                     hide_view("bottom_buttons");
                   break;
           case "unregistered":
                    hide_view("microphone_permission");
                    hide_view("expired_license");
                    hide_view("google_sign_in");
                    hide_view("loading_label");
                    hide_view("status");
                    hide_view("dial_num");
                    show_view("login_screen");
                    hide_view("call_screen");
                    hide_view("select_number");
                    hide_view("settings_screen");
                    hide_view("ask_url");
                     hide_view("phone");
                     hide_view("call_log");
                     hide_view("bottom_buttons");
                   break;
           /*case "email_missing":
                    show_view("email_missing");
                    hide_view("microphone_permission");
                    hide_view("expired_license");
                    hide_view("google_sign_in");
                    hide_view("loading_label");
                    hide_view("status");
                    hide_view("dial_num");
                    hide_view("login_screen");
                    hide_view("call_screen");
                    hide_view("select_number");
                    hide_view("settings_screen");
                    hide_view("ask_url");
                     hide_view("phone");
                     hide_view("call_log");
                     hide_view("bottom_buttons");
                   break;*/
           case "select_number":
                    hide_view("microphone_permission");
                    hide_view("expired_license");
                    hide_view("google_sign_in");
                    hide_view("loading_label");
                    hide_view("status");
                    hide_view("dial_num");
                    hide_view("login_screen");
                    hide_view("call_screen");
                    show_view("select_number");
                    if(insufficient)
                    {
                      show_view("btn_select_num_reset");
                    }
                    else
                    {
                      hide_view("btn_select_num_reset");
                    }
                    append_list("number_list",avl_num_list);
                    hide_view("settings_screen");
                    hide_view("ask_url");
                     hide_view("phone");
                     hide_view("call_log");
                     hide_view("bottom_buttons");
                   break;
           case "incoming":
                    hide_view("microphone_permission");
                  hide_view("expired_license");
                  hide_view("google_sign_in");
                   hide_view("loading_label");
                   hide_view("status");
                   hide_view("dial_num");
                   hide_view("login_screen");
                   show_view('call_screen');
                   show_view('btn_answer');
                   update_log("incoming_alert",incoming_number);
                   update_log("incoming_name",incoming_name);
                   hide_view('select_number');
                   hide_view("settings_screen");
                   hide_view("ask_url");
                    hide_view("phone");
                    hide_view("call_log");
                    hide_view("bottom_buttons");
                   break;
           case "ready":
                    hide_view("microphone_permission");
                  hide_view("expired_license");
                  hide_view("google_sign_in");
                   hide_view("loading_label");
                   hide_view("login_screen");
                   hide_view('call_screen');
                   hide_view('select_number');
                   hide_view("settings_screen");
                   hide_view("ask_url");
                   show_view("phone");
                   hide_view("call_log");
                   update_value("phone_number","");
                   hide_view("back_space");
                   hide_view("btn_cut_call");
                   show_view("btn_make_call");
                   show_view("bottom_buttons");
                   update_bottom_buttons("ready");
                   hide_view("dial_num");
                   show_view("status");
                   //update_log("btn_settings",'<i class="fa fa-cog" aria-hidden="true"></i> &nbsp;settings');
                   break;
           case "call_log":
                    hide_view("microphone_permission");
                  hide_view("expired_license");
                  hide_view("google_sign_in");
                   hide_view("loading_label");
                   hide_view("login_screen");
                   hide_view('call_screen');
                   hide_view('select_number');
                   hide_view("settings_screen");
                   hide_view("ask_url");
                   hide_view("phone");
                   show_view("call_log");
                   hide_view("back_space");
                   hide_view("btn_cut_call");
                   hide_view("btn_make_call");
                   show_view("bottom_buttons");
                   update_bottom_buttons("call_log");
                   hide_view("dial_num");
                   hide_view("status");
                   break;
           case "ask_url":
                    hide_view("microphone_permission");
                  hide_view("expired_license");
                  hide_view("google_sign_in");
                   hide_view("loading_label");
                   hide_view("status");
                   hide_view("login_screen");
                   hide_view('call_screen');
                   hide_view('select_number');
                   hide_view("settings_screen");
                   show_view("ask_url");
                   hide_view("phone");
                   hide_view("call_log");
                   hide_view("bottom_buttons");
                   break;
          case "settings":
                    hide_view("microphone_permission");
                   hide_view("expired_license");
                   hide_view("google_sign_in");
                   hide_view("loading_label");
                   hide_view("status");
                   hide_view("dial_num");
                   hide_view("login_screen");
                   hide_view('call_screen');
                   hide_view('select_number');
                   show_view("settings_screen");
                   update_settings_page();
                   hide_view("ask_url");
                   hide_view("phone");
                   hide_view("call_log");
                   show_view("bottom_buttons");
                   update_bottom_buttons("settings");
                   break;
           case 'dialing':
                    hide_view("microphone_permission");
                    hide_view("expired_license");
                    hide_view("google_sign_in");
                    hide_view("loading_label");
                   hide_view("status");
                   show_view("dial_num");
                   if(outgoing_number!="")
                   {
                    update_log("call_label","Outgoing...");
                    if(incoming_name!="")
                    {
                      update_log("num",incoming_name);
                    }
                    else
                    {
                      update_log("num",outgoing_number);
                    }
                   }
                   if(incoming_number!="")
                   {
                    update_log("call_label","Connected.");
                    if(incoming_name!="")
                    {
                      update_log("num",incoming_name);
                    }
                    else
                    {
                      update_log("num",incoming_number);
                    }
                   }
                   hide_view("login_screen");
                   hide_view('call_screen');
                   hide_view('select_number');
                   hide_view("settings_screen");
                   hide_view("ask_url");
                    show_view("phone");
                   hide_view("call_log");
                    hide_view("back_space");
                    hide_view("btn_make_call");
                    show_view("btn_cut_call");
                    hide_view("bottom_buttons");
                   break;
            default:
                   // update_log("status","Loading...");
                    hide_view("microphone_permission");
                    hide_view("expired_license");
                    hide_view("google_sign_in");
                    show_view("loading_label");
                    hide_view("status");
                    hide_view("dial_num");
                  
                   break;
        }
}

function update_settings_page()
{
      switch (localStorage["crm_type"]) {
         case 'nil':
           crm_show_hide("manual_script");
           if(localStorage.hasOwnProperty("callback_url"))
                update_value("callback_url",localStorage["callback_url"]);
           if(localStorage.hasOwnProperty("domain_url"))
                update_value("domain_url",localStorage["domain_url"]);
           break;
         case '1':
           crm_show_hide("crm_1");
           if(localStorage.hasOwnProperty("api_key_1"))
                update_value("api_key_1",window.atob(localStorage["api_key_1"]));
           break;
         case '2':
           crm_show_hide("crm_2");
           if(localStorage.hasOwnProperty("api_key_2"))
           {
             var api=window.atob(localStorage["api_key_2"]);
             //console.log("api key for agile crm:",api);
             var usr=api.substr(0,api.indexOf(":"));
             var key=api.substr(api.indexOf(":")+1);
            // console.log("usr for agile crm:"+usr);
            // console.log("key for agile crm:"+key);
             update_value("api_key_2_usr",usr);
             update_value("api_key_2_key",key);
           }
                //update_value("api_key_2",window.atob(localStorage["api_key_2"]));
           if(localStorage.hasOwnProperty("agile_domain"))
                update_value("agile_domain",localStorage["agile_domain"]);
           break;
         case '3':
           crm_show_hide("crm_3");
           if(localStorage.hasOwnProperty("api_key_3"))
                update_value("api_key_3",localStorage["api_key_3"]);
           break;
         case '4':
           crm_show_hide("crm_4");
           if(localStorage.hasOwnProperty("salesforce_instance_name") && localStorage.hasOwnProperty("salesforce_client_id") && localStorage.hasOwnProperty("salesforce_client_secret") && localStorage.hasOwnProperty("salesforce_refresh_token"))
                update_value("salesforce_instance_name",localStorage["salesforce_instance_name"]);
                update_value("salesforce_client_id",localStorage["salesforce_client_id"]);
                update_value("salesforce_client_secret",localStorage["salesforce_client_secret"]);
                update_value("api_key_4",localStorage["salesforce_refresh_token"]);
           break;
         case '5':
           crm_show_hide("crm_5");
           if(localStorage.hasOwnProperty("zoho_usr"))
                update_value("zoho_usr",localStorage["zoho_usr"]);
           if(localStorage.hasOwnProperty("zoho_pwd"))
                update_value("zoho_pwd",localStorage["zoho_pwd"]);
           break;
         default:
            localStorage["crm_type"]="select";
            crm_show_hide("");
           break;
      }
       
      update_value("crm_selection",localStorage["crm_type"]);

      if(localStorage["hit_on_ring"]=="true")
      {
         // console.log("hit_on_ring found to be true.");
          update_checkbox("hit_on_ring",true)
      }
      else
      {
         // console.log("hit_on_ring found to be false.");
          update_checkbox("hit_on_ring",false)
      }
       if(localStorage["domain_url"])
            update_value("domain_url",localStorage["domain_url"]);
}

function crm_show_hide(id)
{
  hide_view("domain_manual");
  var crms=["manual_script","manual_script_set","crm_1","crm_1_set","crm_2","crm_2_set","crm_3","crm_3_set","crm_4","crm_4_set","crm_5","crm_5_set"];
  crms.forEach( function(element, index) {
      if(element==id)
      {
        show_view(element);
        if(element=="manual_script")
        {
          show_view("domain_manual");
        }
      }
      else {
        hide_view(element);
      }
  });
}

function update_user_action(screen,action)
{
  //console.log("updating user action");
  var sidd=null,clientnm=null,usr_email=null,chrome_usr_id=null;
  if(localStorage.hasOwnProperty("sid"))
      sidd=localStorage["sid"];
  if(localStorage.hasOwnProperty("identity"))
      clientnm=localStorage["identity"];
  if(localStorage.hasOwnProperty("usr_email"))
      usr_email=localStorage["usr_email"];
  if(localStorage.hasOwnProperty("chrome_usr_id"))
      chrome_usr_id=localStorage["chrome_usr_id"];

  if(localStorage.hasOwnProperty("unique_id"))
  {
      var settings = {
          url: "https://twilioid.com/twilioid/twilio_call_ext_server/user_actions.php",
          method: "POST",
          data: {unique_id:localStorage["unique_id"],sid:sidd,client_name:clientnm,email:usr_email,chrome_usr_id:chrome_usr_id,screen: screen, action: action}
        }
        //console.log("updating user action",settings);
        $.ajax(settings).done(function (response) {
          console.log("user action response",response);
        });
  }
  
}



function get_user_email()
{
  //console.log("getting user email.");
    chrome.identity.getProfileUserInfo(function(userInfo) {
        //console.log("chrome user info",userInfo);
        if(userInfo.id=="" || userInfo.email=="")
        {
           delete localStorage["chrome_usr_id"];
           delete localStorage["usr_email"];
           delete localStorage["license_expired"];
           localStorage["twiliocallislicensed"]="true";
           state="chrome_sign_in";
          // update_ui(state);
        }
        else
        {
            localStorage["usr_email"]=userInfo.email;
            localStorage["chrome_usr_id"]=userInfo.id;
        }
    });
}

function chrome_signIn()
{
  update_user_action("chrome_sign_in","chrome sign in button clicked.");
  //getLicense();
}

function get_microphone_permission()
{
  update_user_action("permission","microphone permission button clicked.");
}



